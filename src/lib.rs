extern crate tokio_core;
extern crate futures;

use tokio_core::reactor::{Core, Handle, Remote, CoreId};
use futures::future::{IntoFuture};
use std::collections::HashMap;
use std::io::Error as IoError;
use std::result::Result as StdResult;
use std::time::Duration;

pub type Result<T> = StdResult<T, Error>;

#[derive(Debug)]
pub enum Error {
    Io(IoError),
    NoWorkersAvailable,
}

impl From<IoError> for Error {
    fn from(err: IoError) -> Self {
        Error::Io(err)
    }
}

struct Worker {
    available: bool,
    remote: Remote,
}

impl Worker {
    fn new(core: &Core) -> Self {
        Self {
            available: true,
            remote: core.remote(),
        }
    }
}

pub struct Executor {
    workers: HashMap<CoreId, Worker>,
}

impl Executor {
    pub fn new(cores: &[Core]) -> Self {
        let workers = cores.iter()
            .map(|core| (core.id(), Worker::new(core)))
            .collect();

        Self {
            workers,
        }
    }

    fn get_available_worker(&self) -> Option<&Worker> {
        self.workers.values().find(|worker| worker.available)
    }

    pub fn spawn<F, R>(&self, future: F) -> Result<()>
        where F: FnOnce(&Handle) -> R + Send + 'static,
              R: IntoFuture<Item = (), Error = ()>,
              R::Future: 'static {
        match self.get_available_worker() {
            Some(worker) => {
                worker.remote.spawn(future);

                Ok(())
            },
            None => Err(Error::NoWorkersAvailable),
        }
    }
}

pub struct Pool {
    pub cores: Vec<Core>,
}

impl Pool {
    pub fn new(pool_size: usize) -> Result<Self> {
        let mut cores = Vec::with_capacity(pool_size);

        for _ in 0..pool_size {
            cores.push(Core::new()?);
        }

        Ok(Self {
            cores,
        })
    }

    pub fn executor(&self) -> Executor {
        Executor::new(&self.cores)
    }

    pub fn turn(&mut self, max_wait: Option<Duration>) {
        self.cores.iter_mut().for_each(|core| core.turn(max_wait));
    }
}

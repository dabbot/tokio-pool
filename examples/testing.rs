extern crate tokio_core;
extern crate futures;
extern crate tokio_pool;

use tokio_pool::Pool;
use futures::future;

fn main() {
    let mut pool = Pool::new(3).unwrap();
    let executor = pool.executor();

    let result = executor.spawn(|_| {
        println!("hello world!");
        future::ok(())
    });

    if let Err(e) = result {
        panic!("oh no! {:?}", e);
    }

    loop {
        pool.turn(None);
    }
}
